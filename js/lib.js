Tetris = {};

Tetris.noviVodnjak = function(x, y) {
    x = x || 10;
    y = y || 20;
    var neke = [];

    for (var i = 1; i <= y; i++) {
        neke[i-1] = [];
        for (var j = 1; j <= x; j++) {
            neke[i-1].push('kocka');
        }
    }
    return neke;
};


Tetris.izpisiTabelo = function(a, selector) {

    var $table = $('<table class="vodnjak"></table>');
    for (var i in a) {
        var row = a[i];
        var $line = $('<tr></tr>');
        for (var j in row) {
            var cell = row[j];
            var $cell = $('<td id="r' + i + '_c' + j + '" class="kocka">&nbsp;</td>');
            if (cell) {
                $cell.addClass(cell);
            }
            $line.append($cell);
        }
        $table.append($line);
    }
    $(selector).append($table);

}


Tetris.randomLik = function() {

    var kateriLik = Math.floor(Math.random() * Tetris.liki.length);

    return Tetris.liki[kateriLik];

};


Tetris.preveriTrk = function(vodnjak, lik, globina, odmik) {

    return false;
};


Tetris.osveziVodnjak = function(vodnjak, lik, globina, odmik) {
    for (var i = 0; i < vodnjak.length; i++) {
        for (var j = 0; j <= vodnjak[i].length; j++) {
            $celica = $('#r' + i + '_c' + j);
            $celica.removeClass(Tetris.classi.join(' ')); 
            $celica.addClass(vodnjak[i][j]);

            if (i >= globina && i <= globina + lik.length - 1) {
                // smo v globini lika
                if (j >= odmik && j <= odmik + lik[0].length - 1) {
                    // smo v širini lika
                    $celica.addClass(lik[i - globina][j - odmik]);
                    
                }
            }
        }
    }
};


// obrni desno

Tetris.obrniDesno = function (a) {
    var b = [];
    for (var i in a[0]) {
        b[i] = a.map(function (j) {
            return j[i];
        }).reverse();
    }
    return b;
}

// obrni levo

Tetris.obrniLevo = function (a) {
    var b = [];
    for (var col in a[0]) {
        var row = a[0].length - 1 - col;
        b[row] = a.map(function(j) {return j[col]});
    }
    return b;
}