Tetris.classi = [
    "lik-modri",
    "lik-vijolicni",
    "lik-rdeci",
    "lik-zlati"
];
Tetris.liki = [
    [
        ['kocka', 'kocka', 'kocka', 'kocka'],
        ['lik-modri', 'lik-modri', 'lik-modri', 'lik-modri'],
        ['kocka', 'kocka', 'kocka', 'kocka'],
        ['kocka', 'kocka', 'kocka', 'kocka']
    ],
    [
        ['kocka', 'kocka', 'kocka', 'kocka'],
        ['lik-vijolcni', 'lik-vijolcni', 'lik-vijolcni', 'kocka'],
        ['kocka', 'lik-vijolcni', 'kocka', 'kocka'],
        ['kocka', 'kocka', 'kocka', 'kocka']
    ],
    [
        ['kocka', 'kocka', 'kocka', 'kocka'],
        ['lik-rdeci', 'lik-rdeci', 'lik-rdeci', 'kocka'],
        ['lik-rdeci', 'kocka', 'kocka', 'kocka'],
        ['kocka', 'kocka', 'kocka', 'kocka']
    ],
    [
        ['lik-modri', 'kocka', 'kocka', 'kocka'],
        ['lik-modri', 'lik-modri', 'lik-modri', 'kocka'],
        ['kocka', 'kocka', 'kocka', 'kocka'],
        ['kocka', 'kocka', 'kocka', 'kocka']
    ],
    [
        ['kocka', 'kocka', 'kocka', 'kocka'],
        ['lik-rdeci', 'lik-rdeci', 'kocka', 'kocka'],
        ['kocka', 'lik-rdeci', 'lik-rdeci', 'kocka'],
        ['kocka', 'kocka', 'kocka', 'kocka']
    ],
    [
        ['kocka', 'kocka', 'kocka', 'kocka'],
        ['kocka', 'lik-zlati', 'lik-zlati', 'kocka'],
        ['lik-zlati', 'lik-zlati', 'kocka', 'kocka'],
        ['kocka', 'kocka', 'kocka', 'kocka']
    ],
    [
        ['kocka', 'lik-rdeci', 'lik-rdeci', 'kocka'],
        ['kocka', 'lik-rdeci', 'lik-rdeci', 'kocka'],
        ['kocka', 'kocka', 'kocka', 'kocka'],
        ['kocka', 'kocka', 'kocka', 'kocka']]
];
